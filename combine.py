import config
import dbf
import glob


def merge_files(dbf_files: list):
    new_file = dbf.Table(dbf_files.pop(0), codepage='cp866')
    new_file.open(mode=dbf.READ_WRITE)
    for file in dbf_files:
        old_file = dbf.Table(file, codepage='cp866', ignore_memos=True)
        old_file.open(mode=dbf.READ_ONLY)
        for row in old_file:
            new_file.append(row)
        old_file.close()
    new_file.close()


if __name__ == "__main__":
    try:
        files = glob.glob(config.PATH + "\\*.dbf")
        merge_files(dbf_files=files)
    except Exception as e:
        print(e)
